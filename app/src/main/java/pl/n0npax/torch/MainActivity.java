package pl.n0npax.torch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends ActionBarActivity {

    private Boolean isOff = true;
    private static Camera camera;
    private final Context context = this;

    @Override
    protected void onStop() {
        super.onStop();
        if (camera != null) {
            camera.release();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera==null) {
            camera = Camera.open();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ImageButton button = (ImageButton) findViewById(R.id.lightButton);
        final ImageView imageView = (ImageView) findViewById(R.id.statusImage);
        final PackageManager pm = context.getPackageManager();

        if(!isCameraSupported(pm)){
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("[ERROR] FLASH or CAMERA");
            alertDialog.setMessage("[ERROR] device doesn't support camera or flash");
            alertDialog.setButton(RESULT_OK, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    finish();
                }
            });
            alertDialog.show();
        }




        camera = Camera.open();
        final Camera.Parameters p = camera.getParameters();

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                isOff = !isOff;
                if (isOff) {
                    imageView.setImageResource(R.drawable.light_off);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                    camera.stopPreview();
                } else {
                    imageView.setImageResource(R.drawable.light_on);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    camera.startPreview();
                }
            }
        });

    }


    private boolean isCameraSupported(PackageManager packageManager){
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) &&
                (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH))) {
            return true;
        }
        return false;
    }



}
